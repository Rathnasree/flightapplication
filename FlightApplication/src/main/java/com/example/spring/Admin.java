package com.example.spring;

public class Admin {
private int flightnumber;
private String airline;
private String fromPlace;
private String toplace;
private String startdatetime,enddatetime;
private String scheduleddays;
private String instrumentused;
private int totalnumberofbusinessclassSeats,nonbusinessclassseats;
private int ticketcost;
private int numberofrows;
public Admin(int flightnumber, String airline, String fromPlace, String toplace, String startdatetime,
		String enddatetime, String scheduleddays, String instrumentused, int totalnumberofbusinessclassSeats,
		int nonbusinessclassseats, int ticketcost, int numberofrows) {
	super();
	this.flightnumber = flightnumber;
	this.airline = airline;
	this.fromPlace = fromPlace;
	this.toplace = toplace;
	this.startdatetime = startdatetime;
	this.enddatetime = enddatetime;
	this.scheduleddays = scheduleddays;
	this.instrumentused = instrumentused;
	this.totalnumberofbusinessclassSeats = totalnumberofbusinessclassSeats;
	this.nonbusinessclassseats = nonbusinessclassseats;
	this.ticketcost = ticketcost;
	this.numberofrows = numberofrows;
}
public int getFlightnumber() {
	return flightnumber;
}
public void setFlightnumber(int flightnumber) {
	this.flightnumber = flightnumber;
}
public String getAirline() {
	return airline;
}
public void setAirline(String airline) {
	this.airline = airline;
}
public String getFromPlace() {
	return fromPlace;
}
public void setFromPlace(String fromPlace) {
	this.fromPlace = fromPlace;
}
public String getToplace() {
	return toplace;
}
public void setToplace(String toplace) {
	this.toplace = toplace;
}
public String getStartdatetime() {
	return startdatetime;
}
public void setStartdatetime(String startdatetime) {
	this.startdatetime = startdatetime;
}
public String getEnddatetime() {
	return enddatetime;
}
public void setEnddatetime(String enddatetime) {
	this.enddatetime = enddatetime;
}
public String getScheduleddays() {
	return scheduleddays;
}
public void setScheduleddays(String scheduleddays) {
	this.scheduleddays = scheduleddays;
}
public String getInstrumentused() {
	return instrumentused;
}
public void setInstrumentused(String instrumentused) {
	this.instrumentused = instrumentused;
}
public int getTotalnumberofbusinessclassSeats() {
	return totalnumberofbusinessclassSeats;
}
public void setTotalnumberofbusinessclassSeats(int totalnumberofbusinessclassSeats) {
	this.totalnumberofbusinessclassSeats = totalnumberofbusinessclassSeats;
}
public int getNonbusinessclassseats() {
	return nonbusinessclassseats;
}
public void setNonbusinessclassseats(int nonbusinessclassseats) {
	this.nonbusinessclassseats = nonbusinessclassseats;
}
public int getTicketcost() {
	return ticketcost;
}
public void setTicketcost(int ticketcost) {
	this.ticketcost = ticketcost;
}
public int getNumberofrows() {
	return numberofrows;
}
public void setNumberofrows(int numberofrows) {
	this.numberofrows = numberofrows;
}
@Override
public String toString() {
	return "Admin [flightnumber=" + flightnumber + ", airline=" + airline + ", fromPlace=" + fromPlace + ", toplace="
			+ toplace + ", startdatetime=" + startdatetime + ", enddatetime=" + enddatetime + ", scheduleddays="
			+ scheduleddays + ", instrumentused=" + instrumentused + ", totalnumberofbusinessclassSeats="
			+ totalnumberofbusinessclassSeats + ", nonbusinessclassseats=" + nonbusinessclassseats + ", ticketcost="
			+ ticketcost + ", numberofrows=" + numberofrows + "]";
}



}
