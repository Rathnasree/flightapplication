package com.example.spring;

public class User {
	private String name;
	private String emailid;
	private String gender;
	private int age;
	private String meal;
	private int seatnumber;
	
	public User(String name, String emailid, String gender, int age, String meal, int seatnumber) {
		super();
		this.name = name;
		this.emailid = emailid;
		this.gender = gender;
		this.age = age;
		this.meal = meal;
		this.seatnumber = seatnumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getMeal() {
		return meal;
	}

	public void setMeal(String meal) {
		this.meal = meal;
	}

	public int getSeatnumber() {
		return seatnumber;
	}

	public void setSeatnumber(int seatnumber) {
		this.seatnumber = seatnumber;
	}
	
}
